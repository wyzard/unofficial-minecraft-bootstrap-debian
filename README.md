Unofficial Package for Minecraft on Debian/Ubuntu
=================================================

This is a package for the [Minecraft][minecraft] bootstrap launcher &mdash; the
small `Minecraft.jar` file that downloads and runs the Minecraft launcher, which
then downloads and runs the game.  Installing this package provides several
benefits over simply downloading the JAR file from the Minecraft website:

 * It adds a Minecraft icon to your desktop environment's application menu, so
   you can launch the game in the same way that you launch all your other
   applications.
 * It also provides a `minecraft` command that you can run from a terminal or in
   a script.  (This is simpler than typing `java -jar /path/to/Minecraft.jar`.)
 * It provides dependency information so the system knows that Java must be
   installed for Minecraft to work.  This helps to ensure that the required Java
   packages are installed, and may allow the system to automatically remove Java
   (if nothing else needs it) when you remove Minecraft.

This package is meant for [Debian][debian] as well as Debian-based distributions
such as [Ubuntu][ubuntu].  It has been tested on Debian 8.4 ("jessie") and
Ubuntu 16.04 LTS ("xenial").

*Note:*  Minecraft is a registered trademark of [Mojang][mojang], Debian is a
registered trademark of [Software in the Public Interest][spi] ("SPI"), and
Ubuntu is a registered trademark of [Canonical][canonical].  This package is
**UNOFFICIAL** and is not produced, endorsed, or supported by Mojang, SPI, or
Canonical.  It has been made merely as a convenience to help install and play
Minecraft on a Debian-based system.

How to Install This Package
---------------------------

This is a *source package*, which is used to build a *binary package* (i.e.
`.deb` file) that can be installed on a Debian-based system.  The binary package
contains proprietary Minecraft files which are owned by Mojang and may not be
distributed without Mojang's permission.  **To avoid infringing Mojang's
copyright, this source package does *not* contain any proprietary files**;
instead, it downloads the necessary files from Mojang while building the binary
package.

This source package can be freely distributed, but the binary package cannot.
In particular, the binary package cannot be placed in a public package
repository for installation with the `apt` tool (the *usual* way of installing
software on a Debian-based system).  Instead, you must build this source package
locally on your own computer to produce your own private copy of the binary
package.  Fortunately, this is easy to do.

To build and install the `unofficial-minecraft-bootstrap` binary package:

 * Install the tools needed to build the source package:
   `sudo apt install build-essential debhelper default-jdk curl`
 * Check out this source package and `cd` to its root directory.
 * Run `fakeroot debian/rules binary`.  This will download `Minecraft.jar` from
   Mojang and package it, together with some other installation files, into a
   `.deb` file in the parent directory.
 * Run `sudo dpkg -i ../unofficial-minecraft-bootstrap_5-1_all.deb` to install
   the binary package that was just built.
 * If the preceding step produces any error messages about dependency problems,
   run `sudo apt install -f` to automatically install the packages that are
   needed.
 * Minecraft is now installed, and you can delete the source package directory
   and the `.deb` file.  You can also uninstall the packages that you installed
   in the first step (`build-essential`, etc.) if you don't need them for
   anything else.

(The directions above assume that the `sudo` program is installed and that your
user account has permission to use it.  If not, use `su -c "(command)"` instead
of `sudo (command)`.)

See [`README.source`](debian/README.source) for more information, including what
to do if the official `Minecraft.jar` download URL has changed since this source
package was created.

**Remember, the `.deb` file contains proprietary Minecraft files and should not
be distributed!**  You can install it on your own computer(s), but don't give it
to others.  Instead, point your friends to this source package so they can build
their own copies of the `.deb` file.  (See the [Mojang brand and asset usage
guidelines][mojang-brand-asset-usage] &mdash; the key requirement is "don't
distribute anything we've made.")

[minecraft]: https://minecraft.net/
[mojang]: http://mojang.com/
[mojang-brand-asset-usage]: https://account.mojang.com/terms#brand
[debian]: https://www.debian.org/
[spi]: http://www.spi-inc.org/
[ubuntu]: http://www.ubuntu.com/
[canonical]: http://www.canonical.com/
