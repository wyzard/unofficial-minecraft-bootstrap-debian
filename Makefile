# Default download URL for Minecraft bootstrap jar.
# Can be overridden from command line or environment.
MINECRAFT_JAR_URL ?= http://s3.amazonaws.com/Minecraft.Download/launcher/Minecraft.jar

# Options for curl can also be set via command line or environment.
MINECRAFT_JAR_CURL_OPTS ?= $(CURL_OPTS)

.PHONY: mojang-files
mojang-files: Minecraft.jar.verified minecraft.png

Minecraft.jar:
	@echo "Downloading Minecraft.jar from Mojang..."
	curl $(MINECRAFT_JAR_CURL_OPTS) $(MINECRAFT_JAR_URL) -R -o Minecraft.jar

Minecraft.jar.verified: Minecraft.jar
	@echo "Verifying contents of Minecraft.jar..."
	@set -e ; \
	sha256sum -c --quiet Minecraft.jar.sha256sum && exit 0 ; \
	echo "Verification failed: Minecraft.jar has unexpected contents." >&2 ; \
	echo "This may mean that Mojang has replaced it with a newer version." >&2 ; \
	echo "To proceed anyway, create the file \"Minecraft.jar.verified\" and try again." >&2 ; \
	echo "(The file's content does not matter, only its existence.)" >&2 ; \
	echo "The resulting package may not work correctly." >&2 ; \
	exit 1
	@touch Minecraft.jar.verified

minecraft.png: Minecraft.jar.verified
	@echo "Extracting Minecraft.jar's favicon.png as minecraft.png..."
	@set -e ; \
	tempdir=$$(mktemp -d extract-icon.XXXXXXXXXX) && \
	cd "$$tempdir" && \
	jar xf ../Minecraft.jar favicon.png && \
	mv favicon.png ../minecraft.png && \
	cd .. && \
	rmdir "$$tempdir"

.PHONY: clean
clean:
	rm -f Minecraft.jar
	rm -f Minecraft.jar.verified
	rm -f minecraft.png
